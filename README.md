# EncryptoDecryptoJS

EncryptoDecryptoJS is a Javascript/Nodejs Package of Classes for creating objects for encryption &amp; decryption of data strings. At the moment, it only uses the aes-192-cbc algorithm and this will probably change in future releases.