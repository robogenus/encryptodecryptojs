const crypto = require('crypto');

// A Class for creating encrypter/decrypter objects that encrypt or decrypt a data-string
// Class_Name: EncrDecrObj - (Calling this class will instantiate a new Encrypter/Decrypter type Object)
class EncrDecrObj {
    constructor(pswrd, datastr) {
        // Class Objects Data Properties
        this.mode = 'encrypt'; // mode `encrypt` is default mode
        this.password = pswrd; // a passphrase for decryption *(!!Note: this needs more work...!)
        this.datastr = datastr; // the string data to encrypt or decrypt
        this.encrypted = 'null'; // the encrypted data *(gets assigned upon object creation)
        this.decrypted = 'null' // 'null' is default before assignment *(gets assigned when encrypted gets decrypted)

        /* encryptian algorithm... *(!!DevNote: Future Version: can be
        passed to constructor but have a default) for now it defaults to 'aes-192-cbc' */
        this.algorithm = 'aes-192-cbc';

        // Key length is dependent on the algorithm. In this case for aes192, it is
        // 24 bytes (192 bits)
        // Use async `crypto.scrypt()` instead.
        // *(!!DevNote: Future Version: this can be passed to constructor but have a default)
        this.key = crypto.scryptSync(this.password, 'salt', 24);

        // Use `crypto.randomBytes()` to generate a random iv instead of the static iv
        this.iv = Buffer.alloc(16, 0); // Initialization Vector
        this.cipher = crypto.createCipheriv(this.algorithm, this.key, this.iv); // cipher encrypter
        this.decipher = crypto.createDecipheriv(this.algorithm, this.key, this.iv); // decipher decrypter
    }

    // Class Object Properties Getter/Setter Methods
    /*
    set mode(mode) { this._mode = mode }
    get mode() { return this._mode; }
    set datastr(datastr) { this._datastr = datastr; };
    get datastr() { return this._datastr; };
    set encrypted(enc) { this._encrypted = enc};
    get encrypted() { return this._encrypted; };
    set decrypted()
    */

    // Encryption Method
    Encrypt() {
        if (this.mode == 'encrypt') {
            let enc = ''; // local var to store encrypted
            let datastr = this.datastr;
            this.cipher.on('readable', () => {
                const data = this.cipher.read();
                if (data)
                    enc += data.toString('hex');

            });
            this.cipher.on('end', () => {
                console.log("ENCRYPTED: "+enc);
                // Prints: ENCRYPTED: <encrypted data string>
            });

            this.cipher.write(datastr);
            this.cipher.end();

            /* ( !!!! )  this.encrypted = enc; */

        } else { console.log("Warning: Wrong mode for 'Encrypt'!") };
    };

    // Decryption Method
    Decrypt() {
        if (this.mode == 'decrypt') {
            let dec = ''; // local var to store decrypted
            this.decipher.on('readable', () => {
                const data = this.decipher.read();
                if (data)
                    dec += data.toString('utf8');
            });
            this.decipher.on('end', () => {
                // console.log("DECRYPTED: "+dec); <--(Remove For Production)
                // Prints: decrypted data
                console.log("DECRYPTED: "+dec);
            });

            const encrypt = this.encrypted;
            this.decipher.write(encrypt, 'hex');
            this.decipher.end();
        } else { console.log("Warning: Wrong mode for 'Decrypt'!") };
    };

}

//##### Code Example: Using (EncrDecrObj Class) -v-v-v- #####//

// calling (EncrDecrObj).Class to create new (EncrDecrObj).Object in (const).myEncrDecrObj --,
const myEncrDecrObj = new EncrDecrObj('abcd123456','Hello, My name is John and I am a programmer');
console.log("Data To Encrypt: "+myEncrDecrObj.datastr);
myEncrDecrObj.Encrypt();    // calling the EncrDecrObj.Encrypt() class method ..to encrypt (EncrDecrObj.datastr) --,
myEncrDecrObj.encrypted = "4c9f4317a5f2b56eeba89ca84ac44660a1b0f4954fb49c422b52026aa992ae4f44b166a1b28f541e38fd0c7c2de364ee";
//console.log("ENCRYPTED: "+myEncrDecrObj.encrypted);
myEncrDecrObj.mode = 'decrypt';
myEncrDecrObj.Decrypt();
