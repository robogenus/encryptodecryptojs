let crypto;
try {
  crypto = require('crypto');
} catch (err) {
  console.log("!Error: 'crypto' support is disabled!");
}

// A Class for creating encrypter/decrypter objects that encrypt or decrypt a data-string
// Class_Name: EncrDecrObj - (Calling this class will instantiate a new Encrypter/Decrypter type Object)
class EncrDecrObj {
  constructor(pswrd, datastr) {
    // Class Objects Data Properties
    this.mode = 'encrypt'; // mode `encrypt` is default mode
    this.password = pswrd; // a passphrase for decryption *(!!Note: this needs more work...!)
    this.datastr = datastr; // the string data to encrypt or decrypt
    this.encrypted = null; // the encrypted data *(gets assigned upon object creation)
    this.decrypted = null; // 'null' is default before assignment *(gets assigned when encrypted gets decrypted)

    /* encryptian algorithm... *(!!DevNote: Future Version: can be
    passed to constructor but have a default) for now it defaults to 'aes-192-cbc' */
    this.algorithm = 'aes-192-cbc';

    // Key length is dependent on the algorithm. In this case for aes192, it is
    // 24 bytes (192 bits)
    // Use async `crypto.scrypt()` instead.
    // *(!!DevNote: Future Version: this can be passed to constructor but have a default)
    this.key = crypto.scryptSync(this.password, 'salt', 24);

    // Use `crypto.randomBytes()` to generate a random iv instead of the static iv
    this.iv = Buffer.alloc(16, 0); // Initialization Vector
    this.cipher = crypto.createCipheriv(this.algorithm, this.key, this.iv); // cipher encrypter
    this.decipher = crypto.createDecipheriv(this.algorithm, this.key, this.iv); // decipher decrypter

    this._debug = false; // If 'true' prints all objects value data to Console, Default=false;
    this.debugdata = null;
  }

  // Class Object Properties Getter/Setter Methods
  set set_mode(mode) { this._mode = mode }
  get get_mode() { return this._mode; }
  set set_data(datastr) { this._datastr = datastr; };
  get get_data() { return this._datastr; };
  set set_encrypted(enc) { this._encrypted = enc};
  get get_encrypted() { return this._encrypted; };
  set set_decrypted(dec) { this._decrypted = dec; };
  get get_decrypted() {return this._decrypted; };
  set set_pswrd(pw) { this.set_pswrd = pw; };
  get get_pswrd() {return this._pswrd; };

  // Encryption Method
  Encrypt() { // Returns Encrypted Data or `null` on fail
    if (this.mode == 'encrypt') {
        let enc = ''; // local var to store encrypted
        let datastr = this.datastr;

        this.cipher.on('readable', () => {
            const data = this.cipher.read();
            if (data)
              enc += data.toString('hex');
            /*this.debugdata = this.debugdata+"\nObject's Encrypt() Method's variable: 'data' = '"+data+"'";*/
        });
        this.cipher.on('end', () => {

          this.debugdata = this.debugdata+"\nObject's Encrypt() Method's variable: 'enc' = '"+enc+"'\nObject's Encrypt() Method's variable: 'datastr' = '"+datastr+"'";

          return this.encrypted; // return encrypted data or null if fail

        });

        this.cipher.write(datastr);
        this.cipher.end();

    } else { console.log("!Error: Wrong EncrDecrObj.mode for 'Encrypt'! Use: EncrDecrObj.mode = 'encrypt';") };
  }

  // Decryption Method
  Decrypt() {
    if (this.mode == 'decrypt') {
        let dec = ''; // local var to store decrypted
        this.decipher.on('readable', () => {
            const data = this.decipher.read();
            if (data) {
              if (this._debug == true) {
                this.debugdata = this.debugdata+"\nObject's Decrypt() Method's variable: 'data' = '"+data+"'";
              }

              dec += data.toString('utf8');
            }
        });
        this.decipher.on('end', () => {

          if (this._debug == true) {
            this.debugdata = this.debugdata+"\nObject's Decrypt() Method's variable: 'dec' = '"+dec+"'";
          }

          return this.decrypted; // return decrypted data or null if fail

        });

        const encrypt = this.encrypted;
        this.decipher.write(encrypt, 'hex');
        this.decipher.end();
    } else { console.log("!Error: Wrong EncrDecrObj.mode for 'Decrypt'! Use: EncrDecrObj.mode = 'decrypt';") };
  };

  _Debug() {

    if (this._debug == true) {
      this.debugdata = this.debugdata +
      "\nmode -> "+this.mode+
      "\npswrd -> "+this.password+
      "\ndatastr -> "+this.datastr+
      "\n_debug -> "+this._debug+
      "\n"+
      "\nData To Encrypt: "+this.get_data+
      "\nENCRYPTED-data: "+this.encrypted+
      "\nDECRYPTED-data: "+this.decrypted+"\n";

      console.log(this.debugdata);
      return true;

    } else {

      console.log("_Debug() Function for this Object is turned off. To enable _Debug() for this Object Set: <YourEncrDecrObjName>._debug = true;");

    }

  }

}



//##### Test Example: Using (EncrDecrObj Class) -v-v-v- #####//
/*
const myEncrDecrObj = new EncrDecrObj('abcd123456','Hello, My name is John and I am a programmer');
myEncrDecrObj._debug = true;
myEncrDecrObj.Encrypt();
console.log(myEncrDecrObj.debugdata);
//myEncrDecrObj._Debug();
*/


// calling (EncrDecrObj).Class to create new (EncrDecrObj).Object in (const).myEncrDecrObj --,
const myEncrDecrObj = new EncrDecrObj('abcd123456','Hello, My name is John and I am a programmer');

// Console-Log: Data to be encrypted
console.log("Data To Encrypt: ''"+myEncrDecrObj.get_data+"''");

// calling the EncrDecrObj.Encrypt() class method ..to encrypt: (myEncrDecrObj.datastr)
myEncrDecrObj.Encrypt();

// myEncrDecrObj.encrypted = "4c9f4317a5f2b56eeba89ca84ac44660a1b0f4954fb49c422b52026aa992ae4f44b166a1b28f541e38fd0c7c2de364ee";
console.log("ENCRYPTED-data: "+myEncrDecrObj.encrypted);
myEncrDecrObj.mode = 'decrypt';
myEncrDecrObj.Decrypt();
console.log("DECRYPTED-data: "+myEncrDecrObj.decrypted);
